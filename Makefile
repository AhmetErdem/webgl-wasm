# Make as a task manager to run cmake to run make.
SRC=$(wildcard app/src/*.cpp)
MAIN=wasm
WASM=build/$(MAIN).html
PORT=8080

.PHONY: wasm
wasm: $(WASM)

# Build WebAssembly and load in Python Webserver
.PHONY: serve
serve: $(WASM)
	cp app/res/html/index.html build
	sed -i -e 's/wasm/$(MAIN)/g' build/index.html
	cd build && python3 ../app/runlocal.py $(PORT)

$(WASM): $(SRC)
	mkdir -p build
	cd build && cmake .. -DCMAKE_TOOLCHAIN_FILE=${EMSCRIPTEN}/cmake/Modules/Platform/Emscripten.cmake
	cd build && make

.PHONY: clean
clean:
	cd build && make clean

.PHONY:
hard_clean:
	rm -rf build
