# WebAssembly WebGL sample project for 3D applications

A starting point for a new web 3D project with webassembly and webgl.

## Details

* Requires `Make` and `CMake` installed.
* `CMake` configures `Make` to build for both environments.
* The `Makefile` in the root is only a task runner.
* A python script is included to run local webserver at 8080 with *.wasm* MIME type.
* Does not depend on SDL, GLFW, GLUT, EGL for rendering context.
* [Libraries available in emscripten][emsdklib].
* Displays an orange triangle on the html canvas. Nothing else.

[emsdklib]:https://github.com/kripken/emscripten/tree/incoming/system/include

## Requirements

* Python script (for local webserver) requires `Python` ***3.6***.
* Uses `Emscripten` version ***1.38.10***.
* Minimum `CMake` version ***3.2***.
* A browser with `WebGL2` support.
* `C++14` compiliant compiler *(this can be modified in the `CMakeLists.cmake` to downgrade to `C++11`)*

## Building

* [Download or Compile the WASM Toolchain][wasm-toolchain].
* Setup the WASM toolchain
  * `cd emsdk`
  * `./emsdk install latest`
  * `./emsdk activate latest`
  * `source ./emsdk_env.sh`
* Build/Run the example
  * `cd webgl-wasm`
  * `make wasm`
  * `make serve`
  * Available at: http://localhost:8080/

[wasm-toolchain]:http://webassembly.org/getting-started/developers-guide/

## Project Structure
* *root* (*Wherever the repo is cloned*)
    * ***app***
        * ***inc*** (*C/C++ header files*)
        * ***lib*** 
            * ***inc*** (*Library API files (i.e. headers)*)
            * ***bin*** (*Compiled Library binaries*)
        * ***src*** (*C/C++ source files*)
        * ***res*** (*Other resources relevant to project*)
            * ***html*** (*html files as resources, index.html*)
        * `runlocal.py` (*python script to run local server at port 8080*)
    * `CMakeLists.cmake` *creates project build*
    * `Makefile` *creates build and allows running simple webserver*

## Notes

The Plan in the begining was to let webserver serve `wasm.html` directly, however current version is using Javascript to HTTP redirect from `index.html` to `wasm.html` 
This project will not work for native builds, it is only to build with Webassembly and WebGL and render on html canvas.

Thanks to:
* https://cmake.org/cmake/help/v3.0/
* https://github.com/eosrei/starter-wasm-webgl-opengl
* https://github.com/DanRuta/webassembly-webgl-shaders
* http://kripken.github.io/emscripten-site/docs/porting/emscripten-runtime-environment.html

## Todo

* Let the `Make` file build in parallel with `nproc`
