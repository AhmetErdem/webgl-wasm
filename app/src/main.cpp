#include <iostream>

// Include the Emscripten library only if targetting WebAssembly
#ifdef __EMSCRIPTEN__
#include <emscripten/emscripten.h>
#include <emscripten/html5.h>
#include <GLES3/gl3.h>
#else
#error EMSCRIPTEN IS NOT FOUND!!
#endif

// Internal Headers
#include "em_utils.hpp"
#include "context.hpp"

static EM_BOOL keyboard_callback(
        int event_type,
        const EmscriptenKeyboardEvent *key_event,
        void *user_data)
{
    fprintf(stdout, "key: %s\n", key_event->key);
    return EM_TRUE;
}

static const char * vert_shader = 
" \
	#version 300 es \n \
	layout (location = 0) in vec3 aPos; \n \
	void main(){ gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0); }	\
";

static const char * frag_shader = 
" \
	#version 300 es \n \
	precision mediump float; \n \
	out vec4 frag_color; \n \
	void main() { frag_color = vec4(1.0, 0.5, 0.2, 1.0); } \
";


static int check_shader(unsigned int id)
{
	int success;
	char info[512];
	glGetShaderiv(id, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(id, 512, nullptr, info);
		std::cout << "ERROR:SHADER::COMPILATION_FAIlED\n" << info << '\n';
	}
	return success;
}

static int check_program(unsigned int id)
{
	int success;
	char info[512];
	glGetProgramiv(id, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(id, 512, nullptr, info);
		std::cout << "ERROR:PROGRAM:LINKING_FAILED\n" << info << '\n';
	}
	return success;
}

unsigned int shader_program;
unsigned int VAO;
unsigned int VBO;

static void create_gl_objs()
{
	static float vertices[] = {
    	-0.5f, -0.5f, 0.0f,
     	0.5f, -0.5f, 0.0f,
     	0.0f,  0.5f, 0.0f
	}; 

	unsigned int vertex_shader;
	unsigned int fragment_shader;	

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);

	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)nullptr);
	glEnableVertexAttribArray(0);

	vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader, 1, &vert_shader, nullptr);
	glCompileShader(vertex_shader);
	check_shader(vertex_shader);

	fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &frag_shader, nullptr);
	glCompileShader(fragment_shader);
	check_shader(fragment_shader);

	shader_program = glCreateProgram();

	glAttachShader(shader_program, vertex_shader);
	glAttachShader(shader_program, fragment_shader);
	glLinkProgram(shader_program);
	check_program(shader_program);

	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);

	glUseProgram(shader_program);
	glBindVertexArray(VAO);

	// glBindBuffer(GL_ARRAY_BUFFER, 0);
	// glBindVertexArray(0);
}

static void destory_gl_objs()
{
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
	glDeleteProgram(shader_program);
}

static void frame_loop(void)
{
  // Clear the window with the background color
  glClear(GL_COLOR_BUFFER_BIT);
  glDrawArrays(GL_TRIANGLES, 0, 3);
}

int main() 
{
  Context wgl_ctx(800,640, nullptr);
  emscripten_webgl_make_context_current(wgl_ctx.get_ctx());

  glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
  
  create_gl_objs();  

  // emscripten_set_keypress_callback(nullptr, nullptr, true, &keyboard_callback);

  emscripten_set_main_loop(frame_loop, -1, true); 

  destory_gl_objs();

  return EXIT_SUCCESS;
}
