// Interface Header
#include "context.hpp"

// System Headers
#include <iostream>

// Toolchain Headers
#include <emscripten/emscripten.h>

// Internal Headers
#include "em_utils.hpp"

Context::Context(const std::int16_t width, const std::int16_t height, char * id)
	: width(width), height(height)
{
	// Context configurations
    EmscriptenWebGLContextAttributes attrs;
    emscripten_webgl_init_context_attributes(&attrs);
    attrs.explicitSwapControl = 0;
    attrs.depth = 1;
    attrs.stencil = 1;
    attrs.antialias = 1;
    attrs.majorVersion = 2;
	attrs.minorVersion = 0;

	gl_ctx_attrs = attrs;

	emscripten_set_canvas_element_size(id, width, height);

	gl_context = emscripten_webgl_create_context(id, &gl_ctx_attrs);
	EM_RESULT_CHECK(gl_context, "wgl context creation failed! ")
}

Context::Context(const std::int16_t width, const std::int16_t height, 
				 char * id, const EmscriptenWebGLContextAttributes & attrs)
	: width(width), height(height), gl_ctx_attrs(attrs)
{
	gl_context = emscripten_webgl_create_context(id, &gl_ctx_attrs);
	EM_RESULT_CHECK(gl_context, "wgl context creation failed! ")
}

Context::~Context()
{
	emscripten_webgl_destroy_context(gl_context);
	EM_RESULT_CHECK(gl_context, "wgl context destruction failed! ")
}

EMSCRIPTEN_WEBGL_CONTEXT_HANDLE Context::get_ctx() const
{
	return gl_context;
}

EmscriptenWebGLContextAttributes Context::get_ctx_attributes() const
{
	return gl_ctx_attrs;
}

std::int16_t Context::get_width() const
{
	return width;
}

std::int16_t Context::get_height() const
{
	return height;
}