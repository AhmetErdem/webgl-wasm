#pragma once

/*
	This header-only file is to beused for short utility function
	to help development with Emscripten

	Current supported version: 1.38.10 (July 27, 2018)
	Author: Ahmet Erdem
*/


#include <emscripten/html5.h>

namespace emscripten_utils
{
	static inline char const*const result2str(const EMSCRIPTEN_RESULT em_result)
	{
		switch(em_result)
		{
			case EMSCRIPTEN_RESULT_SUCCESS: 
				return "EMSCRIPTEN_RESULT_SUCCESS";
			case EMSCRIPTEN_RESULT_DEFERRED: 
				return "EMSCRIPTEN_RESULT_DEFERRED";
			case EMSCRIPTEN_RESULT_NOT_SUPPORTED: 
				return "EMSCRIPTEN_RESULT_NOT_SUPPORTED";
			case EMSCRIPTEN_RESULT_FAILED_NOT_DEFERRED: 
				return "EMSCRIPTEN_RESULT_FAILED_NOT_DEFERRED";
			case EMSCRIPTEN_RESULT_INVALID_TARGET: 
				return "EMSCRIPTEN_RESULT_INVALID_TARGET";
			case EMSCRIPTEN_RESULT_UNKNOWN_TARGET: 
				return "EMSCRIPTEN_RESULT_UNKNOWN_TARGET";
			case EMSCRIPTEN_RESULT_INVALID_PARAM: 
				return "EMSCRIPTEN_RESULT_INVALID_PARAM";
			case EMSCRIPTEN_RESULT_FAILED: 
				return "EMSCRIPTEN_RESULT_FAILED";
			case EMSCRIPTEN_RESULT_NO_DATA: 
				return "EMSCRIPTEN_RESULT_NO_DATA";
			case EMSCRIPTEN_RESULT_TIMED_OUT: 
				return "EMSCRIPTEN_RESULT_TIMED_OUT";
			default: return "EMSCRIPTEN_RESULT_UNKNOWN_VALUE";
		}
	}
}


// CODE GEN MACROS //

#define EM_RESULT_CHECK(R, err_txt) \
	if (R < 0) \
	{ \
		auto r = static_cast<EMSCRIPTEN_RESULT>(R); \
		std::cerr << err_txt \
				  << emscripten_utils::result2str(r) \
				  << '\n'; \
	} 
