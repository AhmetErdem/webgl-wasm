#pragma once

// System Headers
#include <cstdint>

// Toolchain Headers
#include <emscripten/html5.h>

class Context final
{
	private:
		std::int16_t width;
		std::int16_t height;

		EMSCRIPTEN_WEBGL_CONTEXT_HANDLE gl_context;

		EmscriptenWebGLContextAttributes gl_ctx_attrs;

	public:
		Context(std::int16_t width, std::int16_t height, char * id);
		Context(const std::int16_t width, const std::int16_t height, 
			    char * id, const EmscriptenWebGLContextAttributes & attrs);

		~Context();

		EMSCRIPTEN_WEBGL_CONTEXT_HANDLE  get_ctx() const;
		EmscriptenWebGLContextAttributes get_ctx_attributes() const;

		std::int16_t get_width() const;
		std::int16_t get_height() const;
};