import sys
import http.server
import socketserver

if len(sys.argv) < 2:
	PORT = 8080
else:
	PORT = int(sys.argv[1])

class Handler(http.server.SimpleHTTPRequestHandler):
    pass

Handler.extensions_map['.wasm'] = 'application/wasm'

with socketserver.TCPServer(("", PORT), Handler) as httpd:
    print("serving at port", PORT)
    httpd.serve_forever()
